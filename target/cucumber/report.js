$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/featureFiles/finleap.feature");
formatter.feature({
  "name": "Testing finLeap Account Creation functionality",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Enter login with Test Login",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "enter the URL to open the application",
  "keyword": "Given "
});
formatter.match({
  "location": "FinleapStepdefs.java:33"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "get the dummy credential available under the login fields",
  "keyword": "When "
});
formatter.match({
  "location": "FinleapStepdefs.java:37"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "login with that credentials",
  "keyword": "And "
});
formatter.match({
  "location": "FinleapStepdefs.java:40"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify the login is successful",
  "keyword": "Then "
});
formatter.match({
  "location": "FinleapStepdefs.java:43"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Create a account",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "click the create button",
  "keyword": "Then "
});
formatter.match({
  "location": "FinleapStepdefs.java:46"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter personal details to create the account",
  "keyword": "And "
});
formatter.match({
  "location": "FinleapStepdefs.java:49"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click Add button and verify the created account is available",
  "keyword": "Then "
});
formatter.match({
  "location": "FinleapStepdefs.java:53"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Edit and update the account",
  "description": "",
  "keyword": "Scenario"
});
});