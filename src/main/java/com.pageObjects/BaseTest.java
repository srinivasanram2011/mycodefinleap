package com.pageObjects;
import io.cucumber.java.AfterStep;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;


class BaseTest {

    static WebDriver driver;

    @AfterStep
    public void exit() {
        driver.quit();
    }
}
