package com.pageObjects;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;
import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;
import static org.junit.Assert.assertTrue;

public class LoginPage extends BaseTest {
    @FindBy(how= How.XPATH,xpath="//*[@type=\"text\"]")
    static WebElement userID;
    @FindBy(how= How.XPATH,xpath="//*[@type=\"password\"]")
    static WebElement pwd;
    @FindBy(how= How.XPATH,xpath="//*[@type=\"submit\"]")
    static WebElement loginSubmit;
    @FindBy(how=How.XPATH,xpath ="//*[@type=\"submit\"]//following::p[1]" )
    static WebElement getCredentials;

    @FindBy(css = "#greetings")
    static WebElement userLoggedInID;


    public void initialRequest(String browser)
    {

        switch (browser){
            case "CHROME":
                WebDriverManager.getInstance(CHROME).setup();
                DesiredCapabilities acceptSSlCertificate = DesiredCapabilities.chrome();
                acceptSSlCertificate.setCapability( CapabilityType.ACCEPT_SSL_CERTS, true );
                driver = new ChromeDriver( acceptSSlCertificate );
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
                break;
            case "FIREFOX":
                WebDriverManager.firefoxdriver().setup();
                DesiredCapabilities acceptSSlCertificates = DesiredCapabilities.firefox();
                acceptSSlCertificates.setCapability( CapabilityType.ACCEPT_SSL_CERTS, true );
                driver = new FirefoxDriver();
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
                break;

        }
        PageFactory.initElements( driver, LoginPage.class );

    }
    public void launchApplication(String url,String userNames,String password) throws InterruptedException {
        initialRequest("CHROME");
        driver.get( url );
        waitForElement("//*[@type=\"submit\"]");
        userID.sendKeys(userNames);
        pwd.sendKeys(password);
        loginSubmit.click();


       // getDummyCredentials();
        //driver.quit();

    }
    public void loginSuccessful(){
        waitForElement("//*[@id=\"greetings\"]");
        assertTrue("verify right User is logged in",userLoggedInID.isDisplayed());
    }

    public void getDummyCredentials(){
        String getDummyIds=getCredentials.getText();
        //String result =getDummyIds.substring(8,11);
        System.out.println("hello"+getDummyIds.replaceAll("[^9]", ""));

    }
    public void verifyLogInSuccess(){
        waitForElement("(//*[@role=\"dialog\"])[1]");
        //assertTrue("successfully logged in",alertForLog.isDisplayed());

    }

    public void waitForElement(String path){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
    }
}
