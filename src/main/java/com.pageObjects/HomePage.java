package com.pageObjects;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class HomePage extends BaseTest {

    @FindBy(xpath = "//*[@id=\"bAdd\"]")
    static WebElement createButton;
    @FindBy(how= How.CSS,css="[ng-model=\"selectedEmployee.firstName\"]")
    static WebElement firstName;
    @FindBy(css = "[ng-model=\"selectedEmployee.lastName\"]")
    static  WebElement lastName;
    @FindBy(css = "[ng-model=\"selectedEmployee.startDate\"]")
    static WebElement startDate;
    @FindBy(css = "[type=\"email\"]")
    static WebElement email;
    @FindBy(xpath = "(//*[@type=\"submit\"])[2]")
    static WebElement clickAdd;



    public void clickCreateButton(){
        PageFactory.initElements( driver, HomePage.class );
        createButton.isDisplayed();
        createButton.click();

    }
    public void enterDetails(String firstNames,String lastNames,String emails){
        waitForElement("//*[@ng-model=\"selectedEmployee.firstName\"]");
        firstName.sendKeys(firstNames);
        lastName.sendKeys(lastNames);
        Date date= new Date();
        System.out.println(date);
        SimpleDateFormat formatDate=new SimpleDateFormat("YYYY-MM-DD");
        String strDate=formatDate.format(date);
        startDate.sendKeys(strDate);
        email.sendKeys(emails);


    }
    public void clickAddButton(){
        clickAdd.click();
    }
    public void waitForElement(String path){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
    }
    public void closeDriver(){
        driver.quit();
    }


}
