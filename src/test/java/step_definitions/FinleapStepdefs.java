package step_definitions;

import com.pageObjects.HomePage;
import com.pageObjects.LoginPage;
import cucumber.api.java8.En;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class FinleapStepdefs implements En {
    private String URL;
    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private String email;

    public FinleapStepdefs() throws IOException {
        FileReader reader=new FileReader("src/main/java/com.testData/property.properties");
        Properties read=new Properties();
        read.load(reader);
        URL=read.getProperty("url");
        userName=read.getProperty("userName");
        password=read.getProperty("password");
        firstName=read.getProperty("firstName");
        lastName=read.getProperty("lastName");
        email=read.getProperty("email");
        LoginPage loginPageObj=new LoginPage();
        HomePage homePageObj=new HomePage();

        Given("^enter the URL to open the application$", () -> {
            loginPageObj.launchApplication(URL,userName,password);
        });
        When("^get the dummy credential available under the login fields$", () -> {
        });
        And("^login with that credentials$", () -> {
        });
        Then("^verify the login is successful$", () -> {
            loginPageObj.loginSuccessful();
        });
        Then("^click the create button$", () -> {
            homePageObj.clickCreateButton();
        });
        And("^Enter personal details to create the account$", () -> {
            homePageObj.enterDetails(firstName,lastName,email);
        });
        Then("^click Add button and verify the created account is available$", () -> {
            homePageObj.clickAddButton();
            homePageObj.closeDriver();
        });
    }
}
