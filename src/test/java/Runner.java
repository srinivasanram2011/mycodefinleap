
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import io.cucumber.java.AfterStep;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber/report.json"},
        tags = {"not @Ignore"},
        features = "src/test/java/featureFiles/finleap.feature",
        glue = {"step_definitions"},
        junit = {"--step-notifications"})

public class Runner {



}